FROM maven:3.7.2-jdk-8

WORKDIR /ds
COPY . .
EXPOSE 8080
RUN mvn clean install

CMD mvn spring-boot:run